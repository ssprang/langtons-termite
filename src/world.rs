// Copyright (C) 2020 Steve Sprang
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rand::distributions::{Distribution, Uniform};
use std::{char, cmp};

/// Braille bit-to-dot mapping
static BRAILLE_BITS: [[u8; 2]; 4] = [[0x01, 0x08],
				     [0x02, 0x10],
				     [0x04, 0x20],
				     [0x40, 0x80]];

pub type Delta = ((usize, usize), char);

#[inline]
fn byte_to_braille(byte: u8) -> char {
    // 0x2800 is the first Braille Unicode character
    char::from_u32(0x2800 + byte as u32).unwrap()
}

////////////////////////////////////////////////////////////////////////////////
// Cardinal
////////////////////////////////////////////////////////////////////////////////

use crate::world::Cardinal::{N, S, E, W};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Cardinal { N, S, E, W }

impl Cardinal {
    fn flip(self) -> Cardinal {
	match self {
	    N => S, S => N, E => W, W => E
	}
    }

    fn right(self) -> Cardinal {
	match self {
	    N => E, E => S, S => W, W => N
	}
    }

    fn left(self) -> Cardinal {
	match self {
	    N => W, W => S, S => E, E => N
	}
    }
}

////////////////////////////////////////////////////////////////////////////////
// Movement
////////////////////////////////////////////////////////////////////////////////

#[derive(PartialEq, Eq)]
pub enum Movement { Backward, Forward }

impl std::fmt::Display for Movement {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
	match self {
	    Movement::Backward => write!(f, "<-"),
	    Movement::Forward => write!(f, "->"),
	}
    }
}

////////////////////////////////////////////////////////////////////////////////
// World
////////////////////////////////////////////////////////////////////////////////

pub struct World {
    /// Terminal Size
    pub term_size: (usize, usize),
    /// Width in bits
    width: usize,
    /// Height in bits
    height: usize,
    /// Coordinates of current pixel
    x: usize,
    y: usize,
    /// Bytes backing bitmap
    braille_bytes: Vec<Vec<u8>>,
    /// Orientation of termite
    direction: Cardinal,
    /// Invert bits
    pub inverted: bool,
    /// Rotate grid 45 degrees
    pub rotated: bool,
    /// Move forwards or backwards
    pub movement: Movement,
}

impl World {
    pub fn new(term_width: usize, term_height: usize) -> World {
	World {
	    term_size: (term_width, term_height),
	    width: term_width * 2,
	    height: term_height * 4,
	    x: term_width,
	    y: term_height * 2,
	    braille_bytes: vec![vec![0u8; term_width]; term_height],
	    direction: Cardinal::W,
	    inverted: false,
	    rotated: false,
	    movement: Movement::Forward,
	}
    }

    pub fn reset(&mut self) {
	let clear = if self.inverted { 0xff } else { 0x00 };
	let (term_width, term_height) = self.term_size;

	self.width = term_width * 2;
	self.height = term_height * 4;

	// recenter the termite
	self.x = term_width;
	self.y = term_height * 2;

	self.braille_bytes = vec![vec![clear; term_width]; term_height];
	self.direction = Cardinal::W;
    }

    pub fn invert(&mut self) {
	self.inverted = !self.inverted;

	self.braille_bytes.iter_mut()
	    .flatten()
	    .for_each(|byte| *byte ^= 0xff);
    }

    pub fn image(&self) -> String {
	self.braille_bytes.iter()
	    .flatten()
	    .map(|&byte| byte_to_braille(byte))
	    .collect::<String>()
    }

    pub fn image_in_rect(&self, (x, y): (usize, usize), (w, h): (usize, usize)) -> Vec<String> {
	let mut image = Vec::new();

	for row in y..(y + h) {
	    let bytes = &self.braille_bytes[row][x..(x + w)];

	    let line = bytes.iter()
		.map(|&byte| byte_to_braille(byte))
		.collect::<String>();
	    image.push(line);
	}

	image
    }

    fn step(&mut self, direction: Cardinal) {
	// Add the width/height so that we can "go negative" with our unsigned values.
	// These will be kept in the appropriate range by the modulo operator below.
	self.x += self.width;
	self.y += self.height;

	if self.rotated {
	    // angled orientation
	    //   N | E
	    //   --+--
	    //   W | S
	    match direction {
		N => { self.x -= 1; self.y -= 1 }
		S => { self.x += 1; self.y += 1 }
		E => { self.x += 1; self.y -= 1 }
		W => { self.x -= 1; self.y += 1 }
	    }
	} else {
	    // normal orientation
	    //      N
	    //   W--+--E
	    //      S
	    match direction {
		N => self.y -= 1,
		S => self.y += 1,
		E => self.x += 1,
		W => self.x -= 1,
	    }
	}

	self.x %= self.width;
	self.y %= self.height;
    }

    pub fn move_termite(&mut self) -> Delta {
	if self.movement == Movement::Backward {
	    self.step(self.direction.flip());
	}

	let (char_x, char_y) = (self.x / 2, self.y / 4);
	let (bit_x,  bit_y)  = (self.x % 2, self.y % 4);

	let mut byte = self.braille_bytes[char_y][char_x];
	let mask = BRAILLE_BITS[bit_y][bit_x];
	let pixel = byte & mask;

	// turn
	self.direction = if pixel == 0 {
	    self.direction.right()
	} else {
	    self.direction.left()
	};

	// toggle pixel
	byte ^= mask;
	self.braille_bytes[char_y][char_x] = byte;

	if self.movement == Movement::Forward {
	    self.step(self.direction);
	}

	// return the delta
	((char_x, char_y), byte_to_braille(byte))
    }

    pub fn add_dirt(&mut self, amount: usize) {
	let radius = cmp::min(self.width, self.height) as f64 * 0.3;
	let radius_squared = radius * radius;
	let diameter = Uniform::new(-radius, radius);
	let mut rng = rand::thread_rng();
	let mut dots = 0;

	let cx = self.width as f64 / 2.0;
	let cy = self.height as f64 / 2.0;

	// We're generating dots in a square with sides `2r`, but only want dots
	// in the inscribed circle of radius `r`. So, on average we'll have to loop
	// 4/Pi * n times to get n dots in the circle.
	// (area of square / area circle = 4 r^2 / Pi r^2)
	while dots < amount {
	    let x = diameter.sample(&mut rng);
	    let y = diameter.sample(&mut rng);

	    if (x * x + y * y) <= radius_squared {
		let x = (cx + x) as usize;
		let y = (cy + y) as usize;
		// this might turn off a previously turned on dot...
		self.braille_bytes[y / 4][x / 2] ^= BRAILLE_BITS[y % 4][x % 2];
		dots += 1;
	    }
	}
    }
}
