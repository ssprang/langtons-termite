// Copyright (C) 2020 Steve Sprang
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate rand;
extern crate termion;

pub mod controller;
pub mod menu;
pub mod world;

use crate::controller::{Controller, Event, Iteration};
use crate::menu::MenuItem::{self, Action};

use std::io::{stdin, stdout};
use std::time::Duration;
use std::{env, thread};
use std::sync::{Mutex, Arc, mpsc};
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use termion::screen::*;

fn main() {
    let args: Vec<String> = env::args().collect();
    for arg in args {
	if arg == "-V" || arg == "--version" {
	    println!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
	    return;
	}
    }

    let screen = AlternateScreen::from(stdout().into_raw_mode().unwrap());
    let (mut width, mut height) = termion::terminal_size().unwrap();
    // subtract 1 from height to reserve space for status line
    let mut controller = Controller::new(screen, width as usize, height as usize - 1, keymap());

    let event = Arc::new(Mutex::new(None));
    let speed = Arc::clone(&controller.speed);
    let tick = Arc::clone(&event);

    // Generate fast asynchronous `Tick` events.
    thread::spawn(move || {
	loop {
	    match tick.lock() {
		// There will be (at most) a single `Tick` pending no matter how
		// quickly this thread loops. This prevents the UI from becoming
		// unresponsive.
		Ok(mut e) => *e = Some(Event::Tick),
		Err(_) => break
	    }

	    let delay = {
		match speed.lock() {
		    Ok(num) => 4_u64.pow(*num) - 1,
		    Err(_) => break
		}
	    };

	    thread::sleep(Duration::from_micros(delay));
	}
    });

    // use this channel for "slow" events
    let (tx,rx) = mpsc::channel();
    let clock_tx = mpsc::Sender::clone(&tx);

    // Generate asynchronous `Clock` events once per second.
    thread::spawn(move || {
	loop {
	    thread::sleep(Duration::from_secs(1));
	    clock_tx.send(Event::Clock).unwrap();
	}
    });

    // Handle keyboard events asynchronously.
    thread::spawn(move || {
	for c in stdin().keys() {
	    // convert Termion events to our local scheme
	    match c.unwrap() {
		Key::Char(c) => tx.send(Event::Key(c)).unwrap(),
		Key::Left => tx.send(Event::LeftArrowKey).unwrap(),
		Key::Right => tx.send(Event::RightArrowKey).unwrap(),
		_ => {}
	    }
	}
    });

    loop { // event loop
	let (w, h) = termion::terminal_size().unwrap();

	// priorities: terminal resize events > user input || clock > ticks
	let pending = if (width, height) != (w, h) {
	    width = w; height = h;
	    let (w, h) = (w as usize, h as usize - 1);
	    Some(Event::TerminalResize(w, h))
	} else if let Ok(e) = rx.try_recv() {
	    // a `Clock` or keyboard event
	    Some(e)
	} else {
	    // see if we've gotten a `Tick`
	    event.lock().unwrap().take()
	};

	if let Some(e) = pending {
	    if controller.handle_event(e) == Iteration::Break { break }
	}
    }
}

fn keymap() -> Vec<MenuItem> {
    let mut menu = Vec::new();

    menu.push(Action("q/x".to_string(),   "Quit".to_string()));
    menu.push(Action("p".to_string(),     "Pause/Resume".to_string()));
    menu.push(Action("space".to_string(), "Reset".to_string()));
    menu.push(Action("h/?".to_string(),   "Show/Hide Help".to_string()));

    menu.push(MenuItem::Blank);

    menu.push(Action("d".to_string(), "Toggle Dirt".to_string()));
    menu.push(Action("i".to_string(), "Invert".to_string()));
    menu.push(Action("r".to_string(), "Rotate Grid".to_string()));

    menu.push(MenuItem::Blank);

    menu.push(Action("f".to_string(),     "Move Faster".to_string()));
    menu.push(Action("s".to_string(),     "Move Slower".to_string()));
    menu.push(Action("left".to_string(),  "Move Backwards".to_string()));
    menu.push(Action("right".to_string(), "Move Forwards".to_string()));

    menu.push(MenuItem::Separator);
    let mut version = "v".to_string();
    version.push_str(env!("CARGO_PKG_VERSION"));
    menu.push(MenuItem::Version(version));

    menu
}
