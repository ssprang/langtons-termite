// Copyright (C) 2020 Steve Sprang
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::cmp;

// 1 for box edge on each side
const BORDER: usize = 2;

// 1 for single space on each side
const INSET: usize = 2;

// space between left and right portions of menu
const MENU_GAP: usize = 3;

////////////////////////////////////////////////////////////////////////////////
// MenuItem
////////////////////////////////////////////////////////////////////////////////

pub enum MenuItem {
    Action(String, String),
    Blank,
    Separator,
    Version(String),
}

fn calculate_widths(items: &[MenuItem]) -> (usize, usize) {
    let (mut max_left, mut max_right) = (0, 0);

    for item in items.iter() {
	if let MenuItem::Action(left, right) = item {
	    max_left = cmp::max(max_left, left.len());
	    max_right = cmp::max(max_right, right.len());
	}
    }

    (max_left, max_right)
}

////////////////////////////////////////////////////////////////////////////////
// Menu
////////////////////////////////////////////////////////////////////////////////

pub struct Menu {
    pub origin: (usize, usize),
    pub size: (usize, usize),
    pub visible: bool,
    items: Vec<MenuItem>,
    image: Vec<String>,
}

impl Menu {
    pub fn new(items: Vec<MenuItem>, origin: (usize, usize)) -> Menu {
	let (left_width, right_width) = calculate_widths(&items);
	let width = left_width + MENU_GAP + right_width + INSET + BORDER;
	let height = items.len() + BORDER;

	let mut menu = Menu {
	    origin,
	    size: (width, height),
	    visible: false,
	    items,
	    image: vec!(),
	};

	menu.render(left_width, right_width);
	menu
    }

    pub fn contains(&self, x: usize, y: usize) -> bool {
	let (ox, oy) = self.origin;
	let (w, h) = self.size;

	self.visible
	    && x >= ox
	    && x <  ox + w
	    && y >= oy
	    && y <  oy + h
    }

    pub fn extents(&self) -> (usize, usize) {
	let (ox, oy) = self.origin;
	let (w, h) = self.size;

	(ox + w, oy + h)
    }

    pub fn image(&self) -> Vec<String> {
	self.image.clone()
    }

    fn render(&mut self, mut left_width: usize, right_width: usize) {
	let mut rows = Vec::new();
	let (mut width, _height) = self.size;

	left_width += MENU_GAP;
	width -= BORDER;

	let mut line = String::with_capacity(width);
	for _ in 0..width { line.push('─') }

	let mut empty = String::with_capacity(width);
	for _ in 0..width { empty.push(' ') }

	// box parts
	let top =   format!("┌{}┐", line);
	let sep =   format!("├{}┤", line);
	let blank = format!("│{}│", empty);
	let bot =   format!("└{}┘", line);

	rows.push(top);
	for item in self.items.iter() {
	    let m = match item {
		MenuItem::Action(left, right) =>
		    format!("│ {:2$}{:3$} │", left, right, left_width, right_width),
		MenuItem::Version(version) => format!("│{:^1$}│", version, width),
		MenuItem::Blank => blank.clone(),
		MenuItem::Separator => sep.clone(),
	    };
	    rows.push(m);
	}
	rows.push(bot);

	self.image = rows;
    }
}
