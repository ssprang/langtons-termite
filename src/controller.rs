// Copyright (C) 2020 Steve Sprang
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::io::Write;
use std::time::{Duration, Instant};
use std::{char, cmp};
use std::sync::{Mutex, Arc};
use termion::cursor::{self, Goto};

use crate::world::{World, Movement, Delta};
use crate::menu::Menu;
use crate::menu::MenuItem;

/// Thread sleeps for (4^x - 1) microseconds where x <- 0..=MAX_SPEED_STEPS
const MAX_SPEED_STEPS: u32 = 9;
const MAX_DIRT: usize = 500;
const HELP_ORIGIN: (usize, usize) = (2, 1);

////////////////////////////////////////////////////////////////////////////////
// Event
////////////////////////////////////////////////////////////////////////////////

pub enum Event {
    Clock,
    Key(char),
    LeftArrowKey,
    RightArrowKey,
    TerminalResize(usize, usize),
    Tick,
}

////////////////////////////////////////////////////////////////////////////////
// Iteration
////////////////////////////////////////////////////////////////////////////////

#[derive(PartialEq, Eq)]
pub enum Iteration { Break, Continue }

////////////////////////////////////////////////////////////////////////////////
// Elapsed
////////////////////////////////////////////////////////////////////////////////

struct Elapsed(Duration);

impl std::fmt::Display for Elapsed {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
	let mut total = self.0.as_secs();
	let hours = total / 3600;
	total %= 3600;
	let minutes = total / 60;
	let seconds = total % 60;

	if hours == 0 {
	    write!(f, "{}:{:02}", minutes, seconds)
	} else {
	    write!(f, "{}:{:02}:{:02}", hours, minutes, seconds)
	}
    }
}

////////////////////////////////////////////////////////////////////////////////
// Controller
////////////////////////////////////////////////////////////////////////////////

pub struct Controller<W: Write> {
    /// Termite's World
    world: World,
    /// Output
    screen: W,
    /// Terminal width
    width: usize,
    /// Terminal height
    height: usize,
    /// Running time sans pauses
    accumulated_time: Duration,
    current_start_time: Option<Instant>,
    /// Status Variables
    paused: bool,
    add_dirt: bool,
    pub speed: Arc<Mutex<u32>>,
    status_stale: bool,
    /// Average moves over last second
    moves: u64,
    moves_per_sec: u64,
    /// Menu to dislay commands
    help: Menu,
}

impl<W:Write> Controller<W> {
    pub fn new(mut screen: W, width: usize, height: usize, help: Vec<MenuItem>) -> Controller<W> {
	write!(screen, "{}{}", termion::clear::All, cursor::Hide).unwrap();

	Controller {
	    world: World::new(width, height),
	    screen,
	    width,
	    height,
	    accumulated_time: Duration::from_secs(0),
	    current_start_time: Some(Instant::now()),
	    paused: false,
	    add_dirt: false,
	    speed: Arc::new(Mutex::new(MAX_SPEED_STEPS / 3)),
	    status_stale: true,
	    moves: 0,
	    moves_per_sec: 0,
	    help: Menu::new(help, HELP_ORIGIN),
	}
    }

    pub fn handle_event(&mut self, event: Event) -> Iteration {
	match event {
	    Event::TerminalResize(w, h) => self.reset_with_size(w, h),
	    Event::Key(c) => match c {
		' ' => self.reset(),
		'd' => self.toggle_dirt(),
		'f' => self.faster(),
		'i' => self.invert(),
		'p' => self.toggle_pause(),
		'q' | 'x' => return Iteration::Break,
		'r' => self.rotate(),
		's' => self.slower(),
		'?' | 'h' => self.toggle_help(),
		_ => {}
	    }
	    Event::LeftArrowKey => self.move_backward(),
	    Event::RightArrowKey => self.move_forward(),
	    Event::Clock => self.update_mps(),
	    Event::Tick => self.next_frame(),
	}

	if self.status_stale {
	    self.display_status();
	}

	Iteration::Continue
    }
}


////////////////////////////////////////////////////////////////////////////////
// Controller: Actions
////////////////////////////////////////////////////////////////////////////////

impl<W:Write> Controller<W> {
    fn reset(&mut self) {
	self.accumulated_time = Duration::from_secs(0);
	self.current_start_time = if self.paused { None } else { Some(Instant::now()) };

	self.world.reset();
	if self.add_dirt { self.world.add_dirt(MAX_DIRT) }

	self.display_all();
	self.status_stale = true;
    }

    fn reset_with_size(&mut self, width: usize, height: usize) {
	self.width = width;
	self.height = height;
	self.world.term_size = (width, height);

	// hide help if it no longer fits
	if self.help.visible && !self.help_fits() {
	    self.help.visible = false;
	}

	self.reset();
    }

    fn toggle_dirt(&mut self) {
	self.add_dirt = !self.add_dirt;
	self.reset();
    }

    fn faster(&mut self) {
	let mut speed = self.speed.lock().unwrap();
	*speed = cmp::max(0, *speed as i32 - 1) as u32;
	self.status_stale = true;
    }

    fn invert(&mut self) {
	self.world.invert();
	self.display_all();
    }

    fn toggle_pause(&mut self) {
	self.paused = !self.paused;
	self.status_stale = true;

	if self.paused {
	    if let Some(start) = self.current_start_time {
		self.accumulated_time += start.elapsed();
		self.current_start_time = None;
	    }
	} else { // resume
	    self.current_start_time = Some(Instant::now());
	}
    }

    fn rotate(&mut self) {
	self.world.rotated = !self.world.rotated;
	self.reset();
    }

    fn slower(&mut self) {
	let mut speed = self.speed.lock().unwrap();
	*speed = cmp::min(MAX_SPEED_STEPS, *speed + 1);
	self.status_stale = true;
    }

    fn help_fits(&self) -> bool {
	let (max_x, max_y) = self.help.extents();
	max_x <= self.width && max_y <= self.height
    }

    fn toggle_help(&mut self) {
	if !self.help.visible && !self.help_fits() {
	    // don't show help if the terminal is too small
	    return
	}

	self.help.visible = !self.help.visible;

	if self.help.visible {
	    self.display_help();
	} else {
	    // restore world contents
	    self.display_rect(self.help.origin, self.help.size);
	}
    }

    fn move_backward(&mut self) {
	self.world.movement = Movement::Backward;
	self.status_stale = true;
    }

    fn move_forward(&mut self) {
	self.world.movement = Movement::Forward;
	self.status_stale = true;
    }

    fn update_mps(&mut self) {
	self.moves_per_sec = self.moves;
	self.moves = 0;
	self.status_stale = true;
    }

    fn next_frame(&mut self) {
	if !self.paused {
	    let delta = self.world.move_termite();
	    self.display_char(delta);
	    self.moves += 1;
	}
    }
}

////////////////////////////////////////////////////////////////////////////////
// Controller: Display
////////////////////////////////////////////////////////////////////////////////

impl<W:Write> Controller<W> {
    fn display_all(&mut self) {
	let cursor = Goto::default();
	let image = self.world.image();
	write!(self.screen, "{}{}", cursor, image).unwrap();
	if self.help.visible { self.display_help() }
    }

    fn display_char(&mut self, delta: Delta) {
	let ((x, y), braille_char) = delta;

	if !self.help.contains(x, y) {
	    let cursor = Goto(x as u16 + 1, y as u16 + 1);
	    write!(self.screen, "{}{}", cursor, braille_char).unwrap();
	    self.screen.flush().unwrap();
	}
    }

    fn display_image(&mut self, (x, y): (usize, usize), image: Vec<String>) {
	let (x, y) = (x as u16 + 1, y as u16 + 1);

	for (dy, line) in image.iter().enumerate() {
	    let cursor = Goto(x, y + dy as u16);
	    write!(self.screen, "{}{}", cursor, line).unwrap();
	}
	self.screen.flush().unwrap();
    }

    fn display_rect(&mut self, origin: (usize, usize), size: (usize, usize)) {
	let image = self.world.image_in_rect(origin, size);
	self.display_image(origin, image);
    }

    fn display_help(&mut self) {
	let image = self.help.image();
	self.display_image(self.help.origin, image);
    }

    fn elapsed(&self) -> Duration {
	match self.current_start_time {
	    Some(start) => self.accumulated_time + start.elapsed(),
	    None => self.accumulated_time
	}
    }

    fn display_status(&mut self) {
	let speed = *self.speed.lock().unwrap();
	let thumb_ix = MAX_SPEED_STEPS - speed;

	let mut slider = String::with_capacity(MAX_SPEED_STEPS as usize + 1);
	for ix in 0..=MAX_SPEED_STEPS {
	    let c = if ix == thumb_ix { '#' } else { '-' };
	    slider.push(c);
	}

	let status = format!("? for help | angle: {}  movement: {}  ",
			     if self.world.rotated { "45" } else { "0" },
			     self.world.movement);

	let mps = if self.paused {
	    "PAUSED".to_string()
	} else {
	    format!("{:.0} moves/sec", self.moves_per_sec)
	};

	let speed = format!("{}  {}  slow {} fast", mps, Elapsed(self.elapsed()), slider);

	let gap = if self.width > status.len() { self.width - status.len() } else { 0 };
	let mut status_line = format!("{}{:>gap$}", status, speed, gap = gap);
	status_line.truncate(self.width);

	let cursor = Goto(1, self.height as u16 + 1);
	write!(self.screen, "{}{}", cursor, status_line).unwrap();
	self.screen.flush().unwrap();

	self.status_stale = false;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Controller: Drop
////////////////////////////////////////////////////////////////////////////////

impl<W:Write> Drop for Controller<W> {
    fn drop(&mut self) {
	// restore the terminal cursor when we quit
	write!(self.screen, "{}", cursor::Show).unwrap();
    }
}
