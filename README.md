# Langton's Termite

Langton's Termite is an implementation of [Langton's Ant] that runs in a terminal window (see also [Turmite]). It uses [Braille Unicode] characters to simulate a higher resolution bitmap, and it's written in [Rust].

![Langton's Termite](images/screenshot.png) _Terminal: Gnome Terminal, Font: [Anonymous Pro], OS: Arch Linux_

## Running

First, you will need to [install Rust].

Then check out this repository, switch to the checked out directory, and run:

```
cargo run --release
```

## License

Langton's Termite is released under the [GNU General Public License v3].

[Langton's Ant]: https://en.wikipedia.org/wiki/Langton's_ant
[Braille Unicode]: https://en.wikipedia.org/wiki/Braille_Patterns
[Rust]: https://www.rust-lang.org
[Turmite]: https://en.wikipedia.org/wiki/Turmite
[install Rust]: https://www.rust-lang.org/tools/install
[GNU General Public License v3]: https://www.gnu.org/licenses/gpl-3.0.en.html
[Anonymous Pro]: https://www.marksimonson.com/fonts/view/anonymous-pro
